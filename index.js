const express = require('express')
const path = require('path')
require('dotenv').config()

const PORT = process.env.PORT || 3000
const homeRouter = require('./routes/home')

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', homeRouter)

app.listen(PORT, () => {
    console.log(`listening on port ${PORT}`)
})
module.exports = app
